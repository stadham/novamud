from novamud import Dungeon, Room


class BabyRoom(Room):
    pass


class BabyDungeon(Dungeon):

    def init_dungeon(self):
        hr = BabyRoom(self)
        self.start_room = hr


if __name__ == '__main__':
    BabyDungeon().start_dungeon()
