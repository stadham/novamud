from novamud import Dungeon, Room, Thing


class CarKeys(Thing):
    name = 'CarKeys'
    description = "You need to have the car keys before you can leave"


class ApartmentRoom(Room):
    name = 'ApartmentRoom'
    description = ("Spartan but functional. Not particularly clean but that "
                   "can be taken care of before your parents come by for "
                   "your Sunday dinner that you normally hold at your house.")

    def init_room(self):
        self.add_thing(CarKeys())

    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'CarKeys':
            player.tell("You ain't going anywhere without those CarKeys!")
        else:
            super().go_to(player, other_room_name)


class TascaRoom(Room):
    name = 'TascaRoom'
    description = ("Your friendly local Tasca. The wine is cheap and passable "
                   "but certainly nothing special. The bitoque is fantasic.")

    def register_commands(self):
        return [
            'eat_bitoque',
            'drink_wine',
        ]

    def add_player(self, player):
        player.drink_level = 0
        super().add_player(player)

    def eat_bitoque(self, player):
        if player.drink_level > 0:
            player.drink_level -= 1
            player.tell(
                "Much better! Your drink level is now down to {}".format(
                    player.drink_level)
            )
        else:
            player.tell("mmmmmmmm, bitoque!")

    def eat_bitoque_describe(self):
        return "Have a bitoque in case you are getting a bit too tipsy."

    def drink_wine(self, player):
        player.drink_level += 1
        if player.drink_level == 1:
            player.tell('The price makes it taste okay!')
        elif 1 <= player.drink_level < 2:
            player.tell(
                "Your drink level is {}, you're still in the good zone".format(
                    player.drink_level
                )
            )
        elif player.drink_level >= 3:
            player.tell(
                "It might be good to have some food in order to bring that"
                "drink level down a bit... you're currently at drink "
                "level {}".format(player.drink_level)
            )

    def drink_wine_describe(self):
        return ("Have some wine! If you have a bit too much, you can always "
                "eat some food to bring yourself down")


class YoungAdultDungeon(Dungeon):

    name = 'YoungAdultDungeon'
    description = ("You are now a young adult. You have a crappy apartment"
                   "and a car with which you can go to meet up with your "
                   "friends at a TascaRoom.")

    def init_dungeon(self):
        ar = ApartmentRoom(self)
        tr = TascaRoom(self)
        ar.connect_room(tr, two_way=True)
        self.start_room = ar


if __name__ == '__main__':
    YoungAdultDungeon().start_dungeon()
