from novamud import Dungeon, Room


class ToddlerRoom(Room):

    def register_commands(self):
        return ['hello_toddler']

    def hello_toddler(self, player):
        player.tell('toddler: hewo der, {}!'.format(player.name))

    def hello_toddler_describe(self):
        return 'Say hello to the toddler that is in the room.'


class ToddlerDungeon(Dungeon):
    def init_dungeon(self):
        tr = ToddlerRoom(self)
        self.start_room = tr

if __name__ == '__main__':
    ToddlerDungeon().start_dungeon()
