from novamud import Dungeon, Room, Thing


class ColdplayPoster(Thing):
    name = 'ColdplayPoster'
    description = "Much angst, such coldplay"


class TeenagerRoom(Room):
    name = 'TeenagerRoom'
    description = ("A room full of angst. You find a few coldplay posters on"
                   "the wall and it smells slightly of disdain for authority.")

    def init_room(self):
        cp1 = ColdplayPoster()
        cp2 = ColdplayPoster()
        self.add_thing(cp1)
        self.add_thing(cp2)

    def register_commands(self):
        return ['hello_teenager']

    def hello_teenager(self, player):
        player.tell(
            'teenager: ugh. why are you bothering me, {}?'.format(player.name)
        )

    def hello_teenager_describe(self):
        return 'Say hello to the teenager that is in the room.'


class TeenagerDungeon(Dungeon):
    name = 'TeenagerDungeon'
    description = "A dungeon that is as dark as the average teenage soul."

    def init_dungeon(self):
        tr = TeenagerRoom(self)
        self.start_room = tr


if __name__ == '__main__':
    TeenagerDungeon().start_dungeon()
