## Room

### Attributes

#### `description - str`
To be displayed to the user when `describe_room` is called. Use this to
personalize the feel of the room as well as give the user hints as to what
they should do.

#### `name - str`
name of the room

#### `dungeon - str`
The dungeon that the room belongs to. There are a few things that you must do
through the dungeon such as killing a player as well as broadcasting
a message to the entire dungeon.

#### `players - {player_name: Player}`
A mapping from player names to player objects

#### `things - {thing_id: Thing}`
A mapping of thing ids to things

#### `connected_rooms - {room_id: Room}`
map from room id to the room object

#### `commands - [str]`
An array of the commands that you want to open up to the players


### Default commands

All of these are functions that are exposed to the user and may be overridden
by any subclass of `Room`.

#### `pick_up(player, thing_id)`
If the thing with id `thing_id` is present in `self.things` it will be picked
up and added to `player.carrying`.

#### `drop(player)`
If the player is carrying anything, it will be dropped and added back into
the `Room`s `self.things`.

#### `describe_room(player)`
Tells the user how many users / things are in the `Room` as well as what
rooms the current one is connected to.

#### `list_things(player)`
Dumps a list of all of the `Thing` ids that are currently in the `things`
mapping. A user will call this to see what's available so that they can pick
it up.

#### `describe_thing(player, thing_id)`
Sends the `description` string of the `thing_id` to the `player`.

#### `go_to(player, other_room_id)`
Removes the player from the current room and adds them to the other room with
`other_room_id` if it is presend in `self.connected_rooms`. If `other_room_id`
is not present, the command will fail.

#### `show_commands(player)`
Goes through all of the registered commands and prints them along with their
description.

### Non-command functions

#### `init_room()`
This is called every time a Room is created and is where you can set up the
room by doing things such as adding Things or initializing other state.

#### `register_commands() -> [str]`
This is also another command that is called during room initialization and
is how you add new commands to the default ones.

#### `connect_room(other_roomm, two_way=False)`
Calling this with another instiantated `Room` is how you add a door between
the two `Room`s. If you set `two_way=False` it will only create a door from
the current room to the `other_room` and not the other way around. A handy
way to trap players should you be so inclined.

#### `disconnect_room(other_room, two_way=True):`
The opposite of `connect_room`, removes the door. Handy if you want to close
off a door when a certain condition is reached.

#### `broadcast(message, name=None):`
Broadcast a message to everyone in the current room. If you want to prepend
a `name:` to the beginning of the string to attribute a name to the message
set the `name` to be non-null

#### `kill_player(player_name)`
pass
